import styled from 'styled-components';

export const XScroll = styled.div`
	overflow-x: scroll;
`

export const H4 = styled.h4`
	text-decoration: underline;
	margin-bottom: .75rem;
`

export const GrayH4 = styled.h4`
	color: var(--gray);
	textAlign: center;
	margin: 8px;
	text-align: center;
	width: 100%;
`

export const H3 = styled.h3`
	text-decoration: underline;
	margin-bottom: .75rem;
`